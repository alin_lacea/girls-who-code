from django.conf.urls import patterns, include, url
from django.contrib import admin
from movies import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'movies', views.MovieList, base_name="movie")
urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'movielib.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^users', views.UserList.as_view()),
    # url(r'^movies', views.MovieList.as_view()),
    *router.urls
)
