from django.views.decorators.csrf import csrf_exempt
from rest_framework import generics, viewsets
from serializers import UserSerializer, MovieSerializer
from models import User, Movie
from rest_framework.response import Response
from django.core.exceptions import *
from rest_framework import status
from copy import deepcopy
import logging

logger = logging.getLogger("django.models")


class UserList(generics.ListCreateAPIView):
    serializer_class = UserSerializer
    
    def get_queryset(self):
        queryset = User.objects.all()
        username = self.request.QUERY_PARAMS.get('name', None)
        if username:
            queryset = queryset.filter(username__icontains=username)
        return queryset


class MovieList(viewsets.ViewSet):
    
    
    def list(self, request):
        queryset = Movie.objects.all()
        name = request.QUERY_PARAMS.get('name', None)
        listed = request.QUERY_PARAMS.get('list', True)
        if name:
            queryset = queryset.filter(name__icontains=name)
        serializer = MovieSerializer(queryset, many=True)
        # Barbaric unwrap
        if listed in ["false", False]:
            data = serializer.data
        else:
            data = self.unwrap(serializer.data)
        return Response(data)
    
    def create(self, request):
        listed = request.QUERY_PARAMS.get('list', True)
        workaround = []
        logger.info(request.DATA)
        if "user" in request.DATA:
            workaround = request.DATA.pop("user")
        workaround = [workaround] if type(workaround) is not list else workaround
        serializer = MovieSerializer(data=request.DATA)
        if not serializer.is_valid():
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)
        movie = serializer.object
        try:
            old_movie = Movie.objects.get(name__iexact=movie.name)
            
            for user in workaround:
                old_movie.user.add(User.objects.get(pk=user))
            serializer = MovieSerializer(old_movie)
        except ObjectDoesNotExist:
            movie.save()
            for user in workaround:
                try:
                    movie.user.add(User.objects.get(pk=user))
                except ObjectDoesNotExist:
                    movie.delete()
                    return Response("Username {0} not found.".format(user),
                                    status=status.HTTP_404_NOT_FOUND)
        
        if listed in ["false", False]:
            data = serializer.data
        else:
            data = self.unwrap(serializer.data)
        return Response(data)

    
    def unwrap(self, input_data):
        data = []
        processing = [input_data] if type(input_data) is not list else input_data
        logger.info(processing)
        for movie in processing:
            if "user" not in movie or not movie["user"]:
                data.append(movie)
            else:
                for user in movie["user"]:
                    cmovie = deepcopy(movie) 
                    cmovie["user"] = user
                    data.append(cmovie)
        return data