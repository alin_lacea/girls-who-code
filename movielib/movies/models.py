from django.db import models
from django.core.exceptions import *


# Create your models here.
class User(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    username = models.CharField(max_length=100, primary_key=True, editable=False)

    def save(self, *args, **kwargs):
        self.username = "{0}_{1}".format(self.first_name.lower(), self.last_name.lower())
        super(User, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.username

class Movie(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    director = models.CharField(max_length=100, null=True, blank=True)
    genre = models.CharField(max_length=50, null=True, blank=True)
    user = models.ManyToManyField(User)
        
    def __unicode__(self):
        return "{0}: {1}, {2}, {3}".format(self.id, self.name, self.director, self.genre)